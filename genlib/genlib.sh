#!/bin/sh

for BARE in *_BARE.in
do
    basename=`echo $BARE | sed -e 's/_BARE//' -e 's/\.in//'` 
    echo '------ DOING '$basename' ----------------------------------'
    cat BASE.in $BARE  | grep -v '^!' > $basename.in
    feynfig $basename
    rm $basename.in
    echo
done